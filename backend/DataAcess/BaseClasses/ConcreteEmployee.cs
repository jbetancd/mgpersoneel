﻿using System;
namespace DataAcessInterfaces.BaseClasses
{
    public abstract class ConcreteEmployee<HourlyEmployeeObject, MonthlyEmployeeObject>
    {
        public abstract object GetEmployeeObjectFactory(bool getHourlyEmployee);
    }
}
