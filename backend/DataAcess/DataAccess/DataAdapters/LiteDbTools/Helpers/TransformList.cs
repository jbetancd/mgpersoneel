﻿using System.Collections.Generic;
using SharedDto.Contract;

namespace DataAccess.DataAdapters.LiteDbTools.Helpers
{
    public static class TransformList
    {
        public static  List<IEmployeeContract> TransformLiteDbToList( IEnumerable<IEmployeeContract> liteDbCollection)
        {
            List<IEmployeeContract> resultEmployeeList = new List<IEmployeeContract>();
            foreach (IEmployeeContract employee in liteDbCollection)
            {
                resultEmployeeList.Add(employee);
            }
            return resultEmployeeList;
        }
     }
}
