﻿using LiteDB;
using SharedDto.Contract;
using SharedDto;
using DataAccess.DataAdapters.LiteDbTools.DataActions;

namespace DataAccess.DataAdapters.LiteDbTools.Helpers
{
    internal static class  EmployeeHelper
    {
        public static LiteCollection<IEmployeeContract> GetDataCollection(LiteDbConnection connection, string dataCollectionName)
        {
            return connection.StorageConnection.GetCollection<IEmployeeContract>(dataCollectionName);
        }
    }
}


