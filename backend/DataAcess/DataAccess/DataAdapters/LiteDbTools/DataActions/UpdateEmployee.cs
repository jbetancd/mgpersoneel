﻿using DataAccess.DataAdapters.LiteDbTools.Helpers;
using DataAcessInterfaces.Contracts;
using SharedDto.Contract;

namespace DataAccess.DataAdapters.LiteDbTools.DataActions
{
    public class UpdateEmployee : IDataOperation<bool, IEmployeeContract, LiteDbConnection>
    {
        public UpdateEmployee() { }
        public bool ExecuteCommand(LiteDbConnection connection, IEmployeeContract operation)
        {
            var collection = EmployeeHelper.GetDataCollection(connection, SharedDto.Resources.CollectionNameReader.GetEmployeeCollectionName());
            var original = collection.FindById(operation.Id);
            var result = collection.Update(original.Id,operation);
            return result;
        }
    }
}