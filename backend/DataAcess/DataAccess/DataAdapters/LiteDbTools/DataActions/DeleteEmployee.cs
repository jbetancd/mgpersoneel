﻿using DataAccess.DataAdapters.LiteDbTools.Helpers;
using DataAcessInterfaces.Contracts;
using SharedDto.Contract;
using LiteDB;

namespace DataAccess.DataAdapters.LiteDbTools.DataActions
{
    public class DeleteEmployee : IDataOperation<bool, IEmployeeContract, LiteDbConnection>
    {
        public DeleteEmployee() { }
        public bool ExecuteCommand(LiteDbConnection connection, IEmployeeContract employee)
        {
           var recordsDeleted = EmployeeHelper.GetDataCollection(connection, SharedDto.Resources.CollectionNameReader.GetEmployeeCollectionName()).Delete(employee.Id);
           return recordsDeleted ;
        }
    }
}
