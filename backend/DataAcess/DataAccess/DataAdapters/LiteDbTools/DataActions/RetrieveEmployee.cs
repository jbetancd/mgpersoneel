﻿using System;
using DataAcessInterfaces.Contracts;
using SharedDto.Contract;
using System.Collections.Generic;
using DataAccess.DataAdapters.LiteDbTools.Helpers;


namespace DataAccess.DataAdapters.LiteDbTools.DataActions
{
    public class RetrieveEmployee : IDataRetreive<List<IEmployeeContract>, IEmployeeContract, LiteDbConnection>
    {
        public RetrieveEmployee() { }

        public List<IEmployeeContract> GetDataResult(LiteDbConnection connection, IEmployeeContract query)
        {
            if (query == null)
            {
                var collectionAll= EmployeeHelper.GetDataCollection(connection, SharedDto.Resources.CollectionNameReader.GetEmployeeCollectionName()).FindAll();
                var resultAll = TransformList.TransformLiteDbToList(collectionAll);
                return resultAll;
            }
            try
            {
                var collection = EmployeeHelper.GetDataCollection(connection, SharedDto.Resources.CollectionNameReader.GetEmployeeCollectionName()).Find(x => x.EmployeeCode == query.EmployeeCode);
                var result = TransformList.TransformLiteDbToList(collection);
                return result;
            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.Print(exc.ToString());
                return null;
            }
        }
    }
}