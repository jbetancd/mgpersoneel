﻿using DataAccess.DataAdapters.LiteDbTools.Helpers;
using DataAcessInterfaces.Contracts;
using SharedDto.Contract;
using SharedDto.Tools;

namespace DataAccess.DataAdapters.LiteDbTools.DataActions
{
  public   class InsertEmployee : IDataOperation<bool, IEmployeeContract, LiteDbConnection>
    {
        public InsertEmployee() { }

        public bool ExecuteCommand(LiteDbConnection connection, IEmployeeContract employee)
        {
            var dataCollection = EmployeeHelper.GetDataCollection(connection, SharedDto.Resources.CollectionNameReader.GetEmployeeCollectionName());
            employee.Id = dataCollection.Max("_id")+1;
            employee.CalculatedSalary = employee.HourlyContract ?
                            employee.CalculateHourlySalary(employee.BaseSalary)
                            : employee.CalculateMothlySalary(employee.BaseSalary);
            var result = dataCollection.Insert(employee);
            dataCollection.EnsureIndex(x => x.EmployeeCode);
            return true;
        }
    }
}
