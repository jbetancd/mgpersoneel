﻿using DataAcessInterfaces.Contracts;
using LiteDB;
using System;

namespace DataAccess.DataAdapters.LiteDbTools.DataActions
{
   public class LiteDbConnection : IDataConnection<LiteDatabase, string>,IDisposable
    {
        public LiteDbConnection() { }
       
        public LiteDatabase StorageConnection { get ; set ; }

        public bool Connect(string connectionString)
        {
            StorageConnection = new LiteDatabase(connectionString);
            return true; 
        }

        public bool Disconnect()
        {
            StorageConnection.Dispose();
            return true;
        }

        public void Dispose()
        {
            GC.Collect();
        }

        public bool GetStatus()
        {
            return StorageConnection != null;
        }

        
    }
}
