﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessInterfaces.Contracts
{
   public interface IDataConnection<connectionObject,LogErrorObject>
    {
        bool GetStatus();
        connectionObject StorageConnection { get; set; }
        bool Connect(string connectionString);
        bool Disconnect();
    }
}
