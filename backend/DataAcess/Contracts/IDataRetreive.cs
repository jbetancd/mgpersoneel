﻿namespace DataAcessInterfaces.Contracts
{
    public interface IDataRetreive<ResultObject,QueryObject, ConnectionObject>
    {
        ResultObject GetDataResult(ConnectionObject connection, QueryObject query);
    }
}
