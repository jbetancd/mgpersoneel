﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcessInterfaces.Contracts
{
    public interface IDataOperation<resultObject,operationObject,connectionObject>
    {
        resultObject ExecuteCommand(connectionObject connection, operationObject operation);
    }
}
