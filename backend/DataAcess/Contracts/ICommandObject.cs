﻿using System.Collections.Generic;

namespace DataAcessInterfaces.Contracts
{
    public interface ICommandObject<CommandObject,Parameters>
    {
        CommandObject Command { get; set; }
        bool SetParameters(IEnumerable<Parameters> parameters);
    }
}
