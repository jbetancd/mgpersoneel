﻿using System.Collections.Generic;
using System.Web.Http;
using EmployeeBusiness;
using SharedDto.Contract;
using SharedDto.Dto;
using System.Web.Http.Cors;

namespace EmployeeRestApi.Controllers
{
    [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
    [Route("Employee")]
    public class EmployeeController : ApiController
    {
        private static EmployeeService employeeService;

        public EmployeeController()
        {

            employeeService = new EmployeeService($"EmployeeData.db");
        }
        [HttpGet]
        [Route("Test")]
        public string HelloWorld(string test)
        {
            return test;
        }
        [HttpGet]
        [Route("AllEmployees")]
        public List<IEmployeeContract> GetEmployees()
        {
            return employeeService.GetEmployees();
        }
        [HttpGet]
        [Route("Employee")]
        public IEmployeeContract GetEmployee(int employeeCode)
        {
            IEmployeeContract employeeFilter = new EmployeeDto
            {
                ActiveEmployee = true,
                EmployeeCode = employeeCode,
                EmployeeLastName = "",
                EmployeeName = "",
                BaseSalary = 0,
                CalculatedSalary = 0
            };
            List<IEmployeeContract> employees = employeeService.GetEmployeesByFilter(employeeFilter);
            if (employees.Count == 0) return new EmployeeDto();
            return employees[0];
        }
        [HttpPost]
        [Route("Employee")]
        public bool CreateEmployee(EmployeeDto employee)
        {
            List<IEmployeeContract> employeesToCreate = new List<IEmployeeContract>
            {
                employee
            };
            return employeeService.SaveEmployees(employeesToCreate);
        }
        [HttpPut]
        [Route("Employee")]
        public bool UpdateEmployee(EmployeeDto employee)
        {

            return employeeService.UpdateEmployee(employee);
        }
        [HttpDelete]
        [Route("Employee")]
        public bool DeleteEmployee(EmployeeDto employee)
        {
            return employeeService.RemoveEmployee(employee);
        }

    }
}
