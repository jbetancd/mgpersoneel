﻿using System;
using System.Collections.Generic;
using DataAccess.DataAdapters.LiteDbTools.DataActions;
using SharedDto;
using SharedDto.Contract;
using SharedDto.Tools;

namespace EmployeeBusiness
{
    public class EmployeeService
    {
        static LiteDbConnection storageConnection=new LiteDbConnection();
        static RetrieveEmployee retrieveEmployeesResult;
        static UpdateEmployee updateEmployee;
        static DeleteEmployee deleteEmployee;
        static InsertEmployee insertEmployee;
        public EmployeeService(string connectionString)
        {
            storageConnection.Connect(connectionString);
            retrieveEmployeesResult = new RetrieveEmployee();
            updateEmployee = new UpdateEmployee();
            deleteEmployee = new DeleteEmployee();
            insertEmployee = new InsertEmployee();
        }
        public List<IEmployeeContract> GetEmployees()
        {
            return retrieveEmployeesResult.GetDataResult(storageConnection, null);
        }
        public List<IEmployeeContract> GetEmployeesByFilter(IEmployeeContract employeeFilter)
        {

            return retrieveEmployeesResult.GetDataResult(storageConnection, employeeFilter);

        }
        public bool SaveEmployees( List<IEmployeeContract> employeeList)
        {
            bool result=false;
            employeeList.ForEach(x => result =insertEmployee.ExecuteCommand(storageConnection, x));
            return result;
        }
        public bool UpdateEmployee( IEmployeeContract employee)
        {
            bool result = false;
            result = updateEmployee.ExecuteCommand(storageConnection, employee);
            return result;
        }
        public bool RemoveEmployee(IEmployeeContract employee)
        {
            return deleteEmployee.ExecuteCommand(storageConnection, employee);
        }

    }
}
