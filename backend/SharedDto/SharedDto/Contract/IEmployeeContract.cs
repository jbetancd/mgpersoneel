﻿namespace SharedDto.Contract
{
    public interface IEmployeeContract:IEmployee, IContract
    {
       int Id { get; set; }
       int EmployeeCode { get; set; }
    }
}