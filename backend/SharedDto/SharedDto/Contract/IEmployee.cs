﻿namespace SharedDto.Contract
{
    public interface IEmployee
    {
       
        string EmployeeName { get; set; }
        string EmployeeLastName { get; set; }
        bool ActiveEmployee { get; set; }
        bool HourlyContract { get; set; }
    }
}
