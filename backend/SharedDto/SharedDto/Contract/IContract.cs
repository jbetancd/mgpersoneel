﻿namespace SharedDto.Contract
{
    public interface IContract
    {
        double BaseSalary { get; set; }
        double CalculatedSalary { get; set; }
       
    }
}
