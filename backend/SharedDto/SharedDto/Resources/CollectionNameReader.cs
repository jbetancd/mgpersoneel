﻿namespace SharedDto.Resources
{
    public static class CollectionNameReader
    {
        public static string  GetHourlyTotalCollectionName()
        {
            return Resources.Employee.HourlyEmployeesTotal;
        }
        public static string GetMonthlytotalCollectionName()
        {
            return Resources.Employee.MonthlyEmployeesTotal;
        }
        public static string GetEmployeeCollectionName()
        {
            return Resources.Employee.EmployeeRecords;
        }
    }
}
