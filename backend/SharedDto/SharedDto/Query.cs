﻿namespace SharedDto
{
    public class QueryFields
    {
        string FieldToSearch { get; set; }
        string ValueToSearch { get; set; }
    }
}
