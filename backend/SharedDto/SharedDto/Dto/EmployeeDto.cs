﻿using SharedDto.Tools;
using SharedDto.Contract;
namespace SharedDto.Dto
{
    public class EmployeeDto : IEmployeeContract
    {
        public int Id { get ; set ; }
        public int EmployeeCode { get ; set ; }
        public string EmployeeName { get ; set ; }
        public string EmployeeLastName { get ; set ; }
        public bool ActiveEmployee { get ; set ; }
        public bool HourlyContract { get ; set ; }
        public double BaseSalary { get ; set ; }
        public double CalculatedSalary { get ; set ; }
    }
}
