﻿using SharedDto.Contract;
namespace SharedDto.Tools
{
    public static class CalculateEmployeeSalary
    {
        public static  double CalculateHourlySalary(this IEmployeeContract employee,  double baseSalary)
        {
            double.TryParse(Resources.Employee.HoursByMonth, out double hoursByMonth);
            double.TryParse(Resources.Employee.LabourMonthsByYear, out double labourMonthsByYear);
            return  hoursByMonth * baseSalary * labourMonthsByYear;
        }
        public static double CalculateMothlySalary(this IEmployeeContract employee, double baseSalary)
        {
            double.TryParse(Resources.Employee.LabourMonthsByYear, out double labourMonthsByYear);
            return  baseSalary * labourMonthsByYear;
        }
    }
}
