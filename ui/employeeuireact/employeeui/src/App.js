import React, {Component} from 'react';
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  paper: {
    marginTop: theme.spacing(3),
    width: '100%',
    overflowX: 'auto',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 650,
  },
}));



class App extends Component {
  state = {
    employees: []
  }
 searchUrl='http://localhost:50213/Employee?employeeCode={employeeCodeSearchText}';
  componentDidMount() {
    fetch('http://localhost:50213/AllEmployees')
    .then(res => res.json())
    .then((data) => {
      this.setState({ employees: data })
    })
    .catch(console.log)
  }


  
  render () {
    
    return (
      <div>
      
            <Paper className={useStyles.root}>
            <Box color="text.primary" borderColor="blue" border="1pt" clone>
            <div>
                <TextField
                id="employeeCodeSearchText"
                className={useStyles.textField}
                label="Type Employee Code"
                margin="normal"
                />
                <div>
                <Button  color="primary"  className={useStyles.button} align="button">Search</Button>
                </div>
            </div>
            </Box>
            <Table className={useStyles.table} aria-label="simple table">
            <TableHead>
              <TableRow color="primary">
                <TableCell>Row Id</TableCell>
                <TableCell align="right">Employee Code</TableCell>
                <TableCell align="right">Employee First Name</TableCell>
                <TableCell align="right">Employee Last Name</TableCell>
                <TableCell align="right">Hourly Contract</TableCell>
                <TableCell align="right">Base Salary</TableCell>
                <TableCell align="right">calculatedSalary Salary</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.employees.map(row => (
                <TableRow key={row.Id}>
                  <TableCell component="th" scope="row">
                    {row.Id}
                  </TableCell>
                  <TableCell align="right">{row.EmployeeCode}</TableCell>
                  <TableCell align="right">{row.EmployeeName}</TableCell>
                  <TableCell align="right">{row.EmployeeLastName}</TableCell>
                  <TableCell align="right">Off&nbsp;&nbsp;&nbsp;<FormControlLabel
                  control={<Switch checked={row.HourlyContract} color="primary" readOnly />}
                 
                />On</TableCell>
                  <TableCell align="right">{row.BaseSalary}</TableCell>
                  <TableCell align="right">{row.CalculatedSalary}</TableCell>
                </TableRow>
              ))}
            </TableBody>
            </Table>
            </Paper>
            
      </div>
      
    );
  }
}

export default App;


